# Alliance DevLab
Alliance DevLab is devoted to serving the infrastructural needs of co-authoring communities.

## Setup Ubuntu for Local Development:
After a fresh install of Ubuntu simply:

```bash
./setup.sh
```

#### Node development:
```bash
./ubuntu-setup/development/Node.setup.sh
```

#### PHP development:
```bash
./ubuntu-setup/development/PHP.setup.sh
```

#### Rust development:
```bash
./ubuntu-setup/development/Rust.setup.sh
```