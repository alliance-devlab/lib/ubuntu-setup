#! /bin/bash

sudo apt-get update && sudo apt-get dist-upgrade
sudo apt install ubuntu-restricted-extras gnome-tweak-tool lame ffmpeg


sudo add-apt-repository -y universe
sudo add-apt-repository -y ppa:appimagelauncher-team/stable
sudo add-apt-repository -y ppa:ubuntuhandbook1/audacity
sudo apt-get update

sudo apt install exfat-fuse exfat-utils -y

# Install Additional Applications: 
sudo apt install -y appimagelauncher audacity blender thunderbird transmission vlc
sudo snap install bitwarden
sudo snap install telegram-messenger
sudo snap install typora


# Install Brave Browser:
sudo apt install apt-transport-https curl
sudo curl -fsSLo /usr/share/keyrings/brave-browser-archive-keyring.gpg https://brave-browser-apt-release.s3.brave.com/brave-browser-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/brave-browser-archive-keyring.gpg arch=amd64] https://brave-browser-apt-release.s3.brave.com/ stable main"|sudo tee /etc/apt/sources.list.d/brave-browser-release.list
sudo apt update
sudo apt install -y brave-browser


#Install Codium:
wget -qO - https://gitlab.com/paulcarroty/vscodium-deb-rpm-repo/-/raw/master/pub.gpg | gpg --dearmor | sudo dd of=/etc/apt/trusted.gpg.d/vscodium.gpg
echo 'deb https://paulcarroty.gitlab.io/vscodium-deb-rpm-repo/debs/ vscodium main' | sudo tee --append /etc/apt/sources.list.d/vscodium.list
sudo apt update
sudo apt install -y codium

# Install youtube-dl:
sudo curl -L https://yt-dl.org/downloads/latest/youtube-dl -o /usr/local/bin/youtube-dl
sudo chmod a+rx /usr/local/bin/youtube-dl



# Configure:
ln -s ./.local Local
mkdir ~/.local/bin
cp ../local.bin/* ~/.local/bin
chmod +x ~/.local/bin/*

dconf write /org/gnome/desktop/interface/gtk-enable-primary-paste false

cd /usr/local/bin
sudo ln -s $(which youtube-dl) ./dl