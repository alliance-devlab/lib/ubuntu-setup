#! /bin/bash


sudo add-apt-repository -yu ppa:ondrej/php
# sudo apt update
sudo apt install -y software-properties-common

sudo apt install -y php8.0 php-mysql php-cli php-zip php-mbstring php-curl php8.1-dom php-gd php-xml php-tokenizer php-json php-ctype unzip
wget -O composer-setup.php https://getcomposer.org/installer
php ./composer-setup.php --install-dir=$HOME/.local/bin --filename=composer
rm ./composer-setup.php
echo 'deb [trusted=yes] https://repo.symfony.com/apt/ /' | sudo tee /etc/apt/sources.list.d/symfony-cli.list
sudo apt update -y
sudo apt install -y symfony-cli

# Install local certificate authority:
sudo apt install libnss3-tools
symfony server:ca:install
